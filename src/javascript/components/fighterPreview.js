import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfoContainer = createElement({
      tagName: 'div',
      className: 'fighter-preview___info'
    });

    fighterInfoContainer.innerHTML = `
      <span>Name: ${fighter.name}</span>
      <span>Health: ${fighter.health}</span>
      <span>Attack: ${fighter.attack}</span>
      <span>Defense: ${fighter.defense}</span>
    `;

    fighterElement.append(fighterImage, fighterInfoContainer);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
