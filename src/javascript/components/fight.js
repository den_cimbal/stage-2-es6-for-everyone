import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const playerOne = { ...firstFighter };
  const playerTwo = { ...secondFighter };
  
  return new Promise((resolve) => {
    let pressed = new Set();
    let firstFighterHealth = playerOne.health;
    let secondFighterHealth = playerTwo.health;
    const leftFighterIndicator = document.getElementById('left-fighter-indicator');
    const rightFighterIndicator = document.getElementById('right-fighter-indicator');

    playerOne.lastCriticalCall = null;
    playerOne.newCriticalCall = null;
    playerTwo.lastCriticalCall = null;
    playerTwo.newCriticalCall = null;
    
    document.addEventListener('keydown', battle);

    document.addEventListener('keyup', (event) => {
      pressed.delete(event.code);

      if(secondFighterHealth <= 0) {
        document.removeEventListener('keydown', battle);
        resolve(firstFighter);
      } 

      if(firstFighterHealth <= 0) {
        document.removeEventListener('keydown', battle);
        resolve(secondFighter);
      }
    });

    function battle(event) {
      pressed.add(event.code);

      const playerOneAttack = pressed.has(controls.PlayerOneAttack);
      const playerOneBlock = pressed.has(controls.PlayerOneBlock);
      const playerTwoAttack = pressed.has(controls.PlayerTwoAttack);
      const playerTwoBlock = pressed.has(controls.PlayerTwoBlock);
      const playerOneCritical = criticalHitCombination(pressed, controls.PlayerOneCriticalHitCombination);
      const playerTwoCritical = criticalHitCombination(pressed, controls.PlayerTwoCriticalHitCombination);

      if(playerOneAttack && !playerOneBlock) {
        if(!playerTwoBlock) {
          secondFighterHealth -= getDamage(playerOne, playerTwo);
        }

        if(playerTwoAttack) {
          secondFighterHealth -= getDamage(playerOne, playerTwo);
          firstFighterHealth -= getDamage(playerTwo, playerOne);
        }
      } else if(playerTwoAttack && !playerTwoBlock) {
        if(!playerOneBlock) {
          firstFighterHealth -= getDamage(playerTwo, playerOne);
        }
      } else if(playerOneCritical) {
        playerOne.newCriticalCall = new Date();
        secondFighterHealth -= getCriticalDamage(playerOne);
      } else if(playerTwoCritical) {
        playerTwo.newCriticalCall = new Date();
        firstFighterHealth -= getCriticalDamage(playerTwo);
      }

      getFighterHealth(leftFighterIndicator, playerOne.health, firstFighterHealth);
      getFighterHealth(rightFighterIndicator, playerTwo.health, secondFighterHealth);
    }
  });
  
}

export function getDamage(attacker, defender) {
  return Math.max(getHitPower(attacker) - getBlockPower(defender), 0);
}

export function getHitPower(fighter) {
  const criticalHitChance = randomNumber(1, 2);
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomNumber(1, 2);
  const power = fighter.defense * dodgeChance;

  return power;
}

function getCriticalDamage(attacker) {
  if(!attacker.lastCriticalCall || (new Date(attacker.newCriticalCall) - new Date(attacker.lastCriticalCall)) > 10000) {
    attacker.lastCriticalCall = attacker.newCriticalCall;
    return attacker.attack * 2;
  }

  return 0;
}

function criticalHitCombination(pressedKeys, combinationKeys) {
  for(let key of combinationKeys) {
    if(!pressedKeys.has(key)) {
      return;
    }
  }

  return true;
}

function getFighterHealth(indicator, healthBegin, healthLeft) {
  if(healthLeft <= 0) {
    return indicator.style.width = 0;
  }

  return indicator.style.width = healthLeft * 100 / healthBegin + '%';
}

function randomNumber(min, max) {
  return Math.random() * (max - min) + min;
}
